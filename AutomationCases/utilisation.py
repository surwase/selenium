from re import M
from typing import Counter, FrozenSet, Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_util():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    #going to fin section
    driver.get('https://qa.shift.sattva.co.in/client/84/project/304/financials')
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-financials/app-financials-detail/div[2]/div/div[2]/div/div[1]/button/span').click()
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-utilization/div[2]/div[1]/div[1]/div[1]/div/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    driver.find_element_by_xpath('//*[@id="mat-option-0"]/span/span').click()
    #expensecategory
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-utilization/div[2]/div[1]/div[3]/div[1]/div/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    driver.find_element_by_xpath('//*[@id="mat-option-3"]/span').click()
    time.sleep(2)

    #startdate
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-utilization/div[2]/div[1]/div[4]/div[1]/div/div[1]/div/mat-form-field/div/div[1]/div[3]/input').click()
    driver.find_element_by_xpath('//*[@id="mat-datepicker-0"]/div/mat-month-view/table/tbody/tr[2]/td[3]/div').click()
    #enddate
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-utilization/div[2]/div[1]/div[4]/div[1]/div/div[2]/div/mat-form-field/div/div[1]/div[3]/input').click()
    driver.find_element_by_xpath('//*[@id="mat-datepicker-1"]/div/mat-month-view/table/tbody/tr[4]/td[4]/div').click()
    
    input1=driver.find_elements_by_tag_name('input')
    for i in input1:
        print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Particulars':
            i.send_keys(str(today1)+"util")
        if i.get_attribute('placeholder')=='Total Estimate Cost':
            i.send_keys("1000")
        if i.get_attribute('placeholder')=='Total Actual Cost':
            i.send_keys("1000")

    textarea=driver.find_elements_by_tag_name('textarea')
    for i in textarea:
        if i.get_attribute('placeholder')=='Description':
            i.send_keys('description')

    driver.find_element_by_id('fileInput').send_keys("C:/Users/Ashwin Rai/Downloads/Project-Overview.pdf")

    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-utilization/div[3]/div[2]/div/button[2]').click()


    time.sleep(5)
driver.get("https://qa.shift.sattva.co.in/client/list-clients")
create_util()