from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="/usr/bin/chromedriver")
driver.implicitly_wait(10)

today1= date.today()



def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_program():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    #selecting the client
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-list-clients/div[2]/div[2]/div/div/div[1]/mat-card/mat-card-content/div[2]/div[1]').click()
    time.sleep(5)
    #clicking Programs
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav/div/div/mat-nav-list/div[2]/div/mat-accordion/mat-expansion-panel/div/div/a[2]/div/span').click()
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-list-projects/div[2]/div[1]/div/div/div[4]/button').click()
    
    buttons= driver.find_elements_by_tag_name('button')
    for i in buttons:
        if i.get_attribute('role')=='menuitem':
            i.click()
            break
    time.sleep(2)

    #start date
    # for
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[1]/div/div[1]/div/div/div/div[1]/div[2]/mat-form-field/div/div[1]/div[3]/input').click()
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[1]/td[2]/div').click()    
    time.sleep(0.5)
    
    #end date
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[1]/div/div[1]/div/div/div/div[1]/div[3]/mat-form-field/div/div[1]/div[3]/input').click()
    driver.find_element_by_xpath('//*[@id="mat-datepicker-1"]/div/mat-month-view/table/tbody/tr[5]/td[3]/div').click()

    
    #text data
    input1 = driver.find_elements_by_tag_name('input')
    for i in input1:
        # print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Enter Program Name':
            i.send_keys(str(today1))
        if i.get_attribute('placeholder')=='Enter Budget':
            i.send_keys('100000')

    #objective
    textarea1 = driver.find_elements_by_tag_name('textarea')
    for i in textarea1:
        if i.get_attribute('placeholder')=='Program Objective':
            i.send_keys("OBJECTIVE")    
    
    #click next
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[1]/div/div[2]/button[2]').click()
    
    #page2
    #focus area
    driver.find_element_by_xpath("/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div[1]/div/div/div/div/div[1]/div[1]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div").click()

    span1 = driver.find_elements_by_tag_name('span')
    counter = 0
    for i in span1:
        if i.get_attribute('class') == 'mat-option-text':
            i.click()
            counter+=1
            if counter>3:
                break
    driver.find_element_by_xpath("//body").click() #to help close the dropdown menu

    #sub focus
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div[1]/div/div/div/div/div[1]/div[2]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    time.sleep(0.5)
    span2 = driver.find_elements_by_tag_name('span')

    counter = 0
    for i in span2:
        if i.get_attribute('class') == 'mat-option-text':
            i.click()
            counter+=1
            if counter>3:
                break
    driver.find_element_by_xpath("//body").click()
    
    #Target segment
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div[1]/div/div/div/div/div[2]/div[1]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    time.sleep(0.5)
    span3 = driver.find_elements_by_tag_name('span')
    counter = 0
    for i in span3:
        if i.get_attribute('class') == 'mat-option-text':
            i.click()
            counter+=1
            if counter>2:
                break
    driver.find_element_by_xpath("//body").click()
    time.sleep(0.5)
    #sub target
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div[1]/div/div/div/div/div[2]/div[2]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    time.sleep(0.5)
    span4 = driver.find_elements_by_tag_name('span')
    counter = 0
    for i in span4:
        if i.get_attribute('class') == 'mat-option-text':
            i.click()
            counter+=1
            if counter>2:
                break
    driver.find_element_by_xpath("//body").click()

    #schedule7
    counter=0
    span5 = driver.find_elements_by_tag_name('span')
    for i in span5:
        if i.get_attribute('class') == 'mat-checkbox-label':
            i.click()
            counter+=1
            if counter>5:
                break
    #next
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div[2]/button[2]').click()
    
    #click attach file
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[1]/div/div/div/div/div[1]/span[2]/span').click()

    textbox=driver.find_elements_by_tag_name('input')
    for i in textbox:
        if i.get_attribute('placeholder')=='Enter Document Category':
            i.send_keys('doc')

    driver.find_element_by_id('fileInput').send_keys("/home/aarna/Downloads/test")
    time.sleep(2)
    #click upload document
    buttons1=driver.find_elements_by_tag_name('button')
    for i in buttons1:
        if i.text=='Upload Document':
            i.click()

    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-add-plant/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[2]/button[2]').click()
    
    time.sleep(0.1)
    driver.close()
driver.get("https://qa.shift.sattva.co.in/client/list-clients")

create_program()