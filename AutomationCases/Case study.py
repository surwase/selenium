from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_case():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    
    driver.get('https://qa.shift.sattva.co.in/client/84/project/304/impact')
    time.sleep(3)

    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-impact/mat-tab-group/mat-tab-header/div[2]/div/div/div[3]/div/h2').click()
    
    #click create new
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-impact/mat-tab-group/div/mat-tab-body[3]/div/app-impact-case-studies/div/div[1]/div/div/div/div[2]/button/span').click()
    time.sleep(0.5)

    input1=driver.find_elements_by_tag_name('input')
    for i in input1:
        print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Enter Title':
            i.send_keys(str(today1)+" Case") 
    
    textarea = driver.find_elements_by_tag_name('textarea')
    for i in textarea:
        if i.get_attribute('placeholder')=='Enter Case Study Description':
            i.send_keys("Description")
    #pic upload
    driver.find_element_by_id('newFileInput').send_keys('C:/Users/Ashwin Rai/Downloads/picture.jpg')
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[4]/div/mat-dialog-container/app-image-upload/div[3]/div[2]/div/button').click()
    
    driver.find_element_by_id('documentInput').send_keys('C:/Users/Ashwin Rai/Downloads/Project-Overview.pdf')
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-add-impact-case-study-dialog/div[3]/div[2]/div/button[2]').click()

    
    
    
    
    
    
    
    
    time.sleep(10)
try:
    driver.get("https://qa.shift.sattva.co.in/client/list-clients")
    create_case()
    send_status("Creating a case study",today,"Completed","None")
    driver.close()
except:
    send_status("Creating a case study",today,"Error",PrintException())