from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_indicator():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    
    driver.get('https://qa.shift.sattva.co.in/client/84/project/304/impact')
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-impact/mat-tab-group/div/mat-tab-body[1]/div/div/div[1]/button/span').click()

    #freq
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-indication-dialog/div[2]/div[1]/div[1]/div[2]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    time.sleep(4)
    driver.find_element_by_xpath('//*[@id="mat-option-1"]/span').click()
    

    #output
    input1=driver.find_elements_by_tag_name('input')
    for i in input1:
        if i.get_attribute('placeholder')=='Enter Output Indicator':
            i.send_keys('Indicator'+str(today1))
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-indication-dialog/div[2]/div[1]/div[3]/mat-expansion-panel/mat-expansion-panel-header/span[2]').click()

    driver.find_element_by_xpath('//*[@id="mat-input-942"]').send_keys('100')
    driver.find_element_by_xpath('//*[@id="mat-input-943"]').send_keys('100')

    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-indication-dialog/div[3]/div[2]/div/button[2]').click()
    
    


    time.sleep(5)

driver.get("https://qa.shift.sattva.co.in/client/list-clients")
create_indicator()
