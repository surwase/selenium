from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException
option = Options()

option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")

driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
try:    
    driver.get("https://auth.qa.shift.sattva.co.in/auth/realms/sattva/protocol/openid-connect/auth?client_id=sattva-ng&redirect_uri=https%3A%2F%2Fqa.shift.sattva.co.in%2F&state=7c76e629-e2bc-4dd3-bb28-08ca4bc1a530&response_mode=fragment&response_type=code&scope=openid&nonce=ea0cec51-b2ba-49b5-84de-41993a640965")
    #loggin in
    driver.find_element_by_id("username").send_keys("ashwin.rai+10@sattva.co.in")
    driver.find_element_by_id("password").send_keys("October@2021")
    driver.find_element_by_id("kc-login").click()
    time.sleep(3)
    driver.get("https://qa.shift.sattva.co.in/client/73/project/211/project-plan")
    time.sleep(10)
    driver.implicitly_wait(5)

    driver.find_element_by_xpath("/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-project-plan/div/div[2]/div/div/div[3]/button").click()
    element= driver.find_elements_by_tag_name('button')
    # for j in element:
    #     print(i.text)
    # print("end of line")    
    element[-1].click()
    time.sleep(2)

    element1= driver.find_elements_by_tag_name('input')
    # for k in element1:
    #     l= k.get_attribute('placeholder')
    #     print(l)


    #Planned Start Date
    element1[-7].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[1]/td[2]/div").click()
    time.sleep(1)
    #Planned End date
    element1[-6].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[5]/td[5]/div").click()
    #Actual Start
    time.sleep(1)
    element1[-5].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[1]/td[2]/div").click()
    #Actual End Date
    time.sleep(1)
    element1[-4].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[5]/td[5]/div").click()
    #milestone Name
    time.sleep(0.5)
    today=str(time.asctime(time.localtime()))

    element1[-3].send_keys(today)
    #----------#

    element3=driver.find_elements_by_tag_name('mat-select')
    # for l in element3:
    #     m=l.get_attribute('placeholder')
    #     print(m)
    element3[0].click()

    element4= driver.find_elements_by_tag_name('span')
    # for i in element4:
    #     print(i.text)
    element4[-9].click()


    #Sub focus Area
    dropdown = driver.find_elements_by_tag_name('mat-select')
    for i in dropdown:
        if(i.get_attribute('placeholder')=='Select Sub Focus Area'):
            i.click()
            time.sleep(0.5)
            indropdown = driver.find_elements_by_tag_name('mat-option')
            indropdown[-2].click()
            driver.find_element_by_xpath("//body").click()
    #Select Sub Target Segment
        if(i.get_attribute('placeholder')=='Select Sub Target Segment'):
            i.click()
            time.sleep(0.5)
            indropdown = driver.find_elements_by_tag_name('mat-option')
            indropdown[-2].click()
            driver.find_element_by_xpath("//body").click()

    #Description
    element2= driver.find_elements_by_tag_name('textarea')
    # for k in element2:
    #     l= k.get_attribute('placeholder')
    #     print(l)
    element2[-2].send_keys("This is the description")
    #Add Label
    #Add Notes
    element2[-1].send_keys("This is a Note")
    driver.find_element_by_id("fileInput").send_keys("C:/Users/Ashwin Rai/Downloads/file-example_PDF_1MB.pdf")
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-milestone-dialog/div[3]/div[2]/div/button[2]').click()
    time.sleep(10)
    send_status("Creating a Milestone",today,"Completed","None")
except:
    send_status("Creating a Milestone",today,"Error",PrintException())