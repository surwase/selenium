from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()



def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def compliance():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")

    time.sleep(2)
    driver.get("https://qa.shift.sattva.co.in/client/84/overview/landing")
    driver.find_element_by_xpath('//*[@id="cdk-accordion-child-2"]/div/a[3]/div/span').click()    

    time.sleep(1)
    try:
        driver.find_element_by_xpath("/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-compliance/div[2]/mat-card/div/div[3]/button").click()
    except:
        driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-compliance/div[2]/mat-card/div[3]/button[1]').click()
    
    label=driver.find_elements_by_tag_name('label')

    for i in reversed(label):
        i.click()

    time.sleep(0.1)
    buttons=driver.find_elements_by_tag_name('button')
    for i in buttons:
        if i.text=='Submit Evaluation':
            i.click()
    time.sleep(5)



driver.get("https://qa.shift.sattva.co.in/client/list-clients")
compliance()