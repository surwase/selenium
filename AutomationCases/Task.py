from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException
option = Options()

option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")


driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(5)

driver.get("https://auth.qa.shift.sattva.co.in/auth/realms/sattva/protocol/openid-connect/auth?client_id=sattva-ng&redirect_uri=https%3A%2F%2Fqa.shift.sattva.co.in%2F&state=7c76e629-e2bc-4dd3-bb28-08ca4bc1a530&response_mode=fragment&response_type=code&scope=openid&nonce=ea0cec51-b2ba-49b5-84de-41993a640965")




try:
    #logging in
    driver.find_element_by_id("username").send_keys("ashwin.rai+10@sattva.co.in")
    driver.find_element_by_id("password").send_keys("October@2021")
    driver.find_element_by_id("kc-login").click()
    

    time.sleep(3)
    driver.get("https://qa.shift.sattva.co.in/client/73/project/211/project-plan")
    time.sleep(10)

    driver.find_element_by_xpath("/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-project-plan/div/div[2]/div/div/div[3]/button").click()
    element= driver.find_elements_by_tag_name('button')
    # for j in element:
    #     print(i.text)
    # print("end of line")    
    element[-2].click()
    time.sleep(1)

    #Priority Button
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-task-dialog/div[2]/div[1]/div[1]/div/div[1]/button').click()
    priority = driver.find_elements_by_class_name('option-text')
    for i in priority:
        if i.text=='Medium Priority':
            i.click()
    time.sleep(1)
    #task status
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-task-dialog/div[2]/div[1]/div[1]/div/div[2]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    status_options = driver.find_elements_by_class_name('option-text')
    for i in status_options:
        if i.text == 'In Progress':
            i.click()

    #selecting Dates
    element1= driver.find_elements_by_tag_name('input')
    # for i in element1:
    #     print(i.get_attribute('placeholder'))
    #Planned Start Date
    element1[1].click()
    driver.find_element_by_xpath('/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[1]/td[2]/div').click()
    time.sleep(0.5)

    #Planned End date
    element1[2].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[5]/td[5]/div").click()
    #Actual Start
    time.sleep(1)
    element1[3].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[1]/td[2]/div").click()
    #Actual End Date
    time.sleep(1)
    element1[4].click()
    driver.find_element_by_xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody/tr[5]/td[5]/div").click()
    time.sleep(1)
    #Task Name
    today=str(time.asctime(time.localtime()))
    element1[5].send_keys(today)
    #Description and Comment
    element2= driver.find_elements_by_tag_name('textarea')
    for i in element2:
        # print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Enter task Deliverable':
            i.send_keys('This is the Task Deliverable')
        if i.get_attribute('placeholder')=='Add Comment':
            i.send_keys('This is a Comment')    

    #Add more tags
    element1[6].send_keys('TestTag')
    # % Completion
    element1[7].send_keys('80')


    driver.find_element_by_id("fileInput").send_keys("C:/Users/Ashwin Rai/Downloads/file-example_PDF_1MB.pdf")
    time.sleep(0.1)
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-task-dialog/div[3]/div[2]/div/button[2]').click()
    send_status("Creating a Task",today,"Completed","None")
except:
    send_status("Creating a Task",today,"Error",PrintException())


time.sleep(10)
