from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_client():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    
    #click on add a client button
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-list-clients/app-body-heading/div/div[2]/div/button/span/span').click()

    page1=driver.find_elements_by_tag_name('input')
    for i in page1:
        # print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Enter Company Name':
            i.send_keys("Client "+ str(today1))
                
        if i.get_attribute('placeholder')=='Enter Display Name':
            i.send_keys("Client "+ str(today1))
        if i.get_attribute('placeholder')=='Website URL':
            i.send_keys("https://www.sattva.co.in/")
        if i.get_attribute('placeholder')=='Enter Company CIN':
            i.send_keys('123456789')
        if i.get_attribute('placeholder')=='Contact Person Name':
            i.send_keys('Automater')
        if i.get_attribute('placeholder')=='Enter Email Address':
            i.send_keys('abcd@gmail.com')
        if i.get_attribute('placeholder')=='Enter Mobile Number':
            i.send_keys('9876543210')
        if i.get_attribute('placeholder')=='Enter Complete Address here':
            i.send_keys("Client "+ str(today1))
        if i.get_attribute('placeholder')=='Enter Pin Code':
            i.send_keys('201010')
        if i.get_attribute('placeholder')=='Enter State':
            i.send_keys('Uttar Pradesh')
        if i.get_attribute('placeholder')=='Enter City':
            i.send_keys('Lucknow')
    driver.find_element_by_id('fileInput').send_keys('C:/Users/Ashwin Rai/Downloads/picture.jpg')
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/mat-dialog-container/app-image-upload/div[3]/div[2]/div/button').click()
    #click next
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[1]/div/div[2]/button[2]').click()
    page2=driver.find_elements_by_tag_name('textarea')
    for i in page2:
        # print(i.get_attribute('placeholder'))
        if i.get_attribute('placeholder')=='Company Vision':
            i.send_keys('Vision')
        if i.get_attribute('placeholder')=='Company Mission':
            i.send_keys('Mission')
    #click next
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[2]/div/div/button[2]').click()
    
    #dropdown for license type
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[1]/div/div/form/div[1]/div[1]/div/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    page3=driver.find_elements_by_tag_name('span')
    for i in page3:
        if i.text=='Enterprise':
            i.click()
    
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[1]/div/div/form/div[2]/div/div/mat-slide-toggle/label/div/div/div[1]').click()
    
    
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[1]/div/div/form/div[1]/div[2]/div/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    page31=driver.find_elements_by_tag_name('span')
    time.sleep(1)
    for i in page31:
        # print(i.text)
        if i.text=='Standalone':
            i.click()
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-client/app-add-client/div[2]/mat-tab-group/div/mat-tab-body[3]/div/div[2]/button[2]').click()
    time.sleep(10)

try:
    driver.get("https://qa.shift.sattva.co.in/client/list-clients")
    create_client()
    send_status("Creating a Client",today,"Completed","None")
except:
    send_status("Creating a Client",today,"Error",PrintException())
    # PrintException()