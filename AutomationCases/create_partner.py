from re import M
from typing import Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)




def create_partner():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
        
    #click Partner Database
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav/div/div/mat-nav-list/div[3]/div/a/div/span').click()
    #Click Add Partner
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-partner/app-partner-bank/div[2]/div[2]/div/div[2]/button/span').click()
    time.sleep(0.2)
    #Focus area
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/mat-dialog-container/app-create-partner-dialog/div[2]/div[1]/div[2]/div/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]/div').click()
    time.sleep(0.1)
    span_tag=driver.find_elements_by_tag_name('span')
    focus_list=['Arts, Culture and Heritage','Disaster Management','Education', 'Employment', 'Environment']
    for i in span_tag:
        if i.text in focus_list:
            i.click()
            time.sleep(0.1)
    webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
    #enter org name
    input_tags=driver.find_elements_by_tag_name('input')
    time.sleep(0.1)
    for i in input_tags:
        # print(i.get_attibute('placeholder'))
        if i.get_attribute('placeholder')=='Enter Organization Name':
            
            i.send_keys("Auto Client "+ today1.strftime("%B %d %Y"))
            time.sleep(0.5)
        if i.get_attribute('placeholder')=='Select':
            i.send_keys('New')
            span_tag2=driver.find_elements_by_tag_name('span')
            for j in span_tag2:
                if j.text == 'New Test Client.2':
                    j.click()
                    time.sleep(5)
                    break
    #create partner button
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/mat-dialog-container/app-create-partner-dialog/div[3]/div[2]/div/button[2]').click()        
    


try:
    driver.get("https://qa.shift.sattva.co.in/client/list-clients")
    create_partner()
    send_status("Creating a Partner",today,"Completed","None")
except:
    send_status("Creating a Partner",today,"Error",PrintException())
# time.sleep(10)
# create_partner("ashwin.rai@sattva.co.in","July@2021","Auto-Partner")