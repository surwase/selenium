from re import M
from typing import Counter, FrozenSet, Text
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time 
from datetime import date
from selenium.webdriver.support.select import Select
from log_test import send_status
from error_throw import PrintException

today=str(time.asctime(time.localtime()))
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
driver = webdriver.Chrome(options=option, executable_path="C:/Users/Ashwin Rai/Desktop/Sattva/chromedriver/chromedriver.exe")
driver.implicitly_wait(10)

today1= date.today()


def login(username, pwd):
    driver.find_element_by_id("username").send_keys(username)
    driver.find_element_by_id("password").send_keys(pwd)
    driver.find_element_by_id("kc-login").click()
    time.sleep(5)

def create_disbursement():
    if(driver.current_url != "https://qa.shift.sattva.co.in/client/list-clients"):
        login("ashwin.rai+10@sattva.co.in","October@2021")
    #going to fin section
    driver.get('https://qa.shift.sattva.co.in/client/84/project/304/financials')
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/div/app-project/app-financials/app-financials-detail/div[2]/div/div[2]/div/div[2]/button/span').click()
    time.sleep(0.5)
    #status
    driver.find_element_by_xpath('//*[@id="mat-select-0"]/div/div[2]/div').click()
    driver.find_element_by_xpath('//*[@id="mat-option-2"]/span/span').click()
    time.sleep(0.5)
    #reporting period
    driver.find_element_by_xpath('//*[@id="mat-input-0"]').click()
    time.sleep(0.5)
    driver.find_element_by_xpath('//*[@id="mat-datepicker-0"]/div/mat-month-view/table/tbody/tr[2]/td[1]/div').click()
    #to date
    driver.find_element_by_xpath('//*[@id="mat-input-1"]').click()
    driver.find_element_by_xpath('//*[@id="mat-datepicker-1"]/div/mat-month-view/table/tbody/tr[5]/td[2]/div').click()
    #expected date
    driver.find_element_by_xpath('//*[@id="mat-input-5"]').click()
    driver.find_element_by_xpath('//*[@id="mat-datepicker-2"]/div/mat-month-view/table/tbody/tr[4]/td[4]/div').click()
    
    #data filling
    input1=driver.find_elements_by_tag_name('input')
    for i in input1:
        if i.get_attribute('placeholder')=='Enter Expected Amount (INR)':
            i.send_keys('1000')
        if i.get_attribute('placeholder')=='Enter Disbursement Name':
            i.send_keys(str(today1))
    
    textarea=driver.find_elements_by_tag_name('textarea')
    for i in textarea:
        if i.get_attribute('placeholder')=='Enter Disbursement Description':
            i.send_keys('description')
    driver.find_element_by_id('fileInput').send_keys("C:/Users/Ashwin Rai/Downloads/Project-Overview.pdf")
    
    time.sleep(0.5)
    # buttons=driver.find_elements_by_tag_name('button')
    # for i in buttons:
    #     if i.text=='Create Disbursement ':
    #         i.click()
    driver.find_element_by_xpath("//body").click()
    driver.find_element_by_xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/app-create-disbursement-dialog/div[3]/div/div/button[2]').click()
    
    time.sleep(100)
driver.get("https://qa.shift.sattva.co.in/client/list-clients")
create_disbursement()